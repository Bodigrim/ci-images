let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall

let
  BindistSpec: Type =
      { version : Text
      , triple : Text
      }

let Source: Type =
      < FromBindist : BindistSpec
      | FromDistro : List Text
        -- Commands to run to install
      >

let
  Options =
    { Type = 
      { source : Source
      , destDir : Text
      }
    , default = {}
    }

let
  installFromBindistTo: Text -> BindistSpec -> CF.Type =
    \(destDir: Text) -> \(bindist: BindistSpec) ->
      let
        url: Text = "https://github.com/llvm/llvm-project/releases/download/llvmorg-${bindist.version}/clang+llvm-${bindist.version}-${bindist.triple}.tar.xz"

      in
        CF.run "install LLVM"
        [ "curl -L ${url} | tar -xJC ."
        , "mkdir ${destDir}"
        , "cp -R clang+llvm*/* ${destDir}"
        , "rm -R clang+llvm*"
        , "${destDir}/bin/llc --version"
        ]

let
  install: Options.Type -> CF.Type =
    \(opts: Options.Type) ->
      merge
      { FromBindist = installFromBindistTo opts.destDir
      , FromDistro = \(cmds: List Text) -> CF.run "install LLVM" cmds
      } opts.source

let
  maybeInstallTo: Text -> Optional Source -> CF.Type =
    \(destDir: Text) -> \(source: Optional Source) ->
      merge
      { Some = \(src: Source) -> install { source = src, destDir = destDir }
      , None = [] : CF.Type
      } source

let
  setEnv: Text -> CF.Type =
    \(destDir: Text) ->
      CF.env (toMap
        { LLC = "${destDir}/bin/llc"
        , OPT = "${destDir}/bin/opt"
        })

in
{ Options = Options
, Source = Source
, BindistSpec = BindistSpec
, install = install
, maybeInstallTo = maybeInstallTo
, setEnv = setEnv
}
